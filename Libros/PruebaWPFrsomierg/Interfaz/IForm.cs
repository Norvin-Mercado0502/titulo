﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libros.Interfaces
{
    public interface IForm<T>
    {
        void SetData(T data);
        T GetData();
    }
}
